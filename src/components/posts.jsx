import React, {useEffect, useState} from 'react';
import axios from 'axios';
import _ from 'lodash';

const pageSize = 20;

const Posts = () => {

    const [posts, setposts] = useState();
    const [paginatedPosts, setpaginatedPosts] = useState();
    const [currentPage, setcurrentPage] = useState(1);
    const [Keyword, setKeyword] = useState("");
    const [totalfilteredPosts, settotalfilteredPosts] = useState();
    const [filteredPosts, setfilteredPosts] = useState();
    const [filteredPages, setfilteredPages] = useState();

    useEffect(()=>{
        axios.get('https://api.coingecko.com/api/v3/coins/list')
        .then(res=>{
            setposts(res.data);
            setpaginatedPosts(_(res.data).slice(0).take(pageSize).value());
        })

    },[]);

    const pageCount = posts? Math.ceil(posts.length/pageSize) :0;
    var filteredPageCount = 0;

    if(pageCount === 1){
        return null;
    }

    const pages = _.range(1, pageCount + 1);

    const pagination=(pageNo)=>{


        if (Keyword != "") {

            setcurrentPage(pageNo);
            const startIndex = (pageNo - 1) * pageSize;
            const paginatedPost = _(totalfilteredPosts).slice(startIndex).take(pageSize).value();

            setfilteredPosts(paginatedPost);

        }

        else{

            setcurrentPage(pageNo);
            const startIndex = (pageNo - 1) * pageSize;
            const paginatedPost = _(posts).slice(startIndex).take(pageSize).value();
            setpaginatedPosts(paginatedPost);

        }


    }


    const filterFunction= () => {

        const subsetOfPosts = [];

        for(var i =0; i< posts.length; i++){

            if(posts[i].name.toLowerCase().includes(Keyword.toLocaleLowerCase())){
                subsetOfPosts.push(posts[i])
            }

        }

        filteredPageCount = subsetOfPosts? Math.ceil(subsetOfPosts.length/pageSize) :0;
        setfilteredPages(_.range(1, filteredPageCount + 1));
        settotalfilteredPosts(subsetOfPosts);
        setfilteredPosts(_(subsetOfPosts).slice(0).take(pageSize).value());
    }



    return <div>

        <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', padding: '20px'}}>
            List of All the Available Crypto Currencies at CoinGecko</div>

        <div style={{padding: "50px"}}>
            {

                !paginatedPosts ? ("CoinGecko free API only Allows 50 calls per minute " +
                    "that's why there is no data, please try and refresh again after one minute, thanks. "):
                    (

                        <div>


                            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', paddingBottom: '40px'}}>

                                <input
                                    key="random1"
                                    value={Keyword}
                                    placeholder={"Filter By Name"}
                                    onChange={
                                        (e) =>
                                        {
                                            setKeyword(e.target.value);
                                            filterFunction();

                                        }
                                    }
                                />

                            </div>



                            <div>
                                {(() => {
                                    if (Keyword != "") {
                                        return (
                                            <table className='table'>
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ID</th>
                                                    <th>Symbol</th>
                                                    <th>Name</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                {
                                                    filteredPosts.map((post, index)=>(
                                                            <tr key={index}>
                                                                <td>{(index + 1) + ((currentPage * pageSize) - pageSize)}</td>
                                                                <td>{post.id}</td>
                                                                <td>{post.symbol}</td>
                                                                <td>{post.name}</td>
                                                            </tr>
                                                        )
                                                    )
                                                }
                                                </tbody>
                                            </table>
                                        )
                                    }

                                     else {
                                        return (
                                            <table className='table'>
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ID</th>
                                                    <th>Symbol</th>
                                                    <th>Name</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                {
                                                    paginatedPosts.map((post, index)=>(
                                                            <tr key={index}>
                                                                <td>{(index + 1) + ((currentPage * pageSize) - pageSize)}</td>
                                                                <td>{post.id}</td>
                                                                <td>{post.symbol}</td>
                                                                <td>{post.name}</td>
                                                            </tr>
                                                        )
                                                    )
                                                }
                                                </tbody>
                                            </table>
                                        )
                                    }
                                })()}
                            </div>















                        </div>

        )}
        </div>

        <nav className="d-flex justify-content-center" style={{paddingBottom:'200px'}}>

            <div>
                {(() => {
                    if (Keyword != "") {
                        return (
                            <select className="pagination">
                                {
                                    //filteredPages

                                    filteredPages.map((page)=>(
                                        <option className="page-link" onClick={()=>pagination(page)} >
                                            {page}
                                        </option>
                                    ))
                                }
                            </select>
                        )
                    } else {
                        return (
                            <select className="pagination">
                                {
                                    pages.map((page)=>(
                                        <option className="page-link" onClick={()=>pagination(page)} >
                                            {page}
                                        </option>
                                    ))
                                }
                            </select>
                        )
                    }
                })()}
            </div>


        </nav>
        

    </div>
    
    
}
 
export default Posts