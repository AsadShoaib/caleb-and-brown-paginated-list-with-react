import logo from './logo.svg';
import './App.css';
import Posts from './components/posts';

function App() {
  return (
    <div>
      <h1 style={{display: 'flex',  justifyContent:'center', alignItems:'center', paddingTop: '20px'}}>Caleb And Brown</h1>
      <Posts/>
    </div>
  );
}

export default App;
